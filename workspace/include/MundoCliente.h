//Code autor: Fco. Javier Valero Cuadrado.

// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"
#include "Socket.h"
#include <pthread.h>

class CMundo_Cliente 
{
public:
	void Init();
	CMundo_Cliente();
	virtual ~CMundo_Cliente();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	Esfera disparo1;
	Esfera disparo2;
	
	int puntos1;
	int puntos2;
	char Jug1[100]="Jugador 1 marca 1 punto,";
	char Jug2[100]="Jugador 2 marca 1 punto,";
	//----------------Conexion Servidor-------------------------
	int fd1;
	char cadena1[100];
	char fifo1[100]="FIFO_Datos";
	Socket s;
	//----------------Envio de teclas-------------------------
	int fd2;
	int rf;
	char cadena2[100];
	char fifo2[100]="FIFO_Teclas";
	//char tecla[10]="0";
	void SendComandosJugador();
	pthread_attr_t attr;
	pthread_t thid1;
	//-------------------------BOT----------------------------
	DatosMemCompartida *mem;
	DatosMemCompartida datoscomp;
	
	DatosMemCompartida *mem2;
	DatosMemCompartida datoscomp2;
	//-------------------------Contador----------------------
	int contador;
	int flag_nobot=1;
	
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
