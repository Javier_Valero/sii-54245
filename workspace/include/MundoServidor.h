//Code autor: Fco. Javier Valero Cuadrado.

// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"
#include "Socket.h"
#include <pthread.h>


class CMundo_Servidor
{
public:
	void Init();
	CMundo_Servidor();
	virtual ~CMundo_Servidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	Esfera disparo1;
	Esfera disparo2;
	
	int puntos1;
	int puntos2;
	char Jug1[100]="Jugador 1 marca 1 punto,";
	char Jug2[100]="Jugador 2 marca 1 punto,";
	//-----------------LOGGER---------------------------------
	int fd;
	int rf;
	char cadena[200];
	char fin[100]="FIN DEL JUEGO\n\0";
	char logger[100]="fifo_logger";
	//-----------------Conexion Cliente---------------------------------
	int fd2;
	char cadena2[100];
	char fifo[100]="FIFO_Datos";
	//-----------------Recibir teclas---------------------------------
	int fd3;
	pthread_t thid1;
	pthread_t thid2;
	char cadena3[100];
	char fifo3[100]="FIFO_Teclas";
	void RecibeComandosJugador1();
	void RecibeComandosJugador2();
	//-------------------------Contador----------------------
	int contador;
	int flag_nobot=1;
	//-----------------Socket----------------------------
	Socket s;
	pthread_t thid_socket;
  	 std::vector<Socket> s_client;
  	 void GestionaConexiones();
  	 int acabar=0;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
