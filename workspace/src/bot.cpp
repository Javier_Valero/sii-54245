#include"DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>




int main(){
	int fd;
	DatosMemCompartida *datosmem;
	int fd2;
	DatosMemCompartida *datosmem2;
	//Abro1
	fd=open("Datos_Compartidos.txt",O_RDWR);
	if (fd < 0) {
           perror("Error en la apertura del fichero 1");
           exit(1);
        }
        datosmem=static_cast<DatosMemCompartida*>(mmap(0, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0));
        //Abro2
        fd2=open("Datos_Compartidos2.txt",O_RDWR);
	if (fd2 < 0) {
           perror("Error en la apertura del fichero 2");
           exit(1);
        }
        datosmem2=static_cast<DatosMemCompartida*>(mmap(0, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd2, 0));
if (datosmem2==MAP_FAILED){
		perror("Error en la proyeccion del fichero 2");
		close(fd2);
		return(1);
	}
	close(fd);
	close(fd2);
	
	while(1){
		
		if((datosmem->raqueta1.y1 < datosmem->esfera.centro.y+0.5)&&(datosmem->raqueta1.y1 > datosmem->esfera.centro.y-0.5)){
			datosmem->accion=0;
		}else if(datosmem->raqueta1.y1 > datosmem->esfera.centro.y){
			datosmem->accion=-1;
		}else if(datosmem->raqueta1.y1 < datosmem->esfera.centro.y){
			datosmem->accion=1;

		}
		if((datosmem2->raqueta1.y1 < datosmem2->esfera.centro.y+0.8)&&(datosmem2->raqueta1.y1 > datosmem2->esfera.centro.y-0.8)){
			datosmem2->accion=0;
		}else if(datosmem2->raqueta1.y1 > datosmem2->esfera.centro.y){
			datosmem2->accion=-1;
		}else if(datosmem2->raqueta1.y1 < datosmem2->esfera.centro.y){
			datosmem2->accion=1;
		}
		usleep(25000);
		
	}
	munmap(datosmem,sizeof(DatosMemCompartida));
	munmap(datosmem2,sizeof(DatosMemCompartida));
	return 1;
}
