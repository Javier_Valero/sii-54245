//Code autor: Fco. Javier Valero Cuadrado.

// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos(void* d);

CMundo_Cliente::CMundo_Cliente()
{
	datoscomp.esfera=esfera;
	datoscomp.raqueta1=jugador1;
	datoscomp.accion=0;
	datoscomp2.esfera=esfera;
	datoscomp2.raqueta1=jugador1;
	datoscomp2.accion=0;
	Init();
	disparo1.setPos(-300, -300);
	disparo1.setRadio(0);
	disparo1.setVel(0, 0);
	disparo2.setPos(-300, -300);
	disparo2.setRadio(0);
	disparo2.setVel(0, 0);

}

CMundo_Cliente::~CMundo_Cliente()
{
	close(fd1);
	close(fd2);
	munmap(mem,sizeof(DatosMemCompartida));
	munmap(mem2,sizeof(DatosMemCompartida));
	unlink("Datos_Compartidos2.txt");
	unlink("Datos_Compartidos.txt");
	unlink(fifo1);
	unlink(fifo2);

}

void CMundo_Cliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
	
	contador=0;


//MMAP1
	int fd_mmap;
	printf("Voy a abrir fich\n");
	fd_mmap= open("Datos_Compartidos.txt", O_CREAT|O_TRUNC|O_RDWR, 0666);
	if (fd_mmap < 0) {
           perror("Error creación fichero destino");
           exit(1);
        }
	printf("Voy a escribir en fich\n");
	write(fd_mmap,&datoscomp,sizeof(datoscomp));
	mem=static_cast<DatosMemCompartida*> (mmap(NULL,sizeof(datoscomp),PROT_READ|PROT_WRITE,MAP_SHARED,fd_mmap,0));
	if(mem==MAP_FAILED){
        perror("mmap");
        return;
        }
	
	close(fd_mmap);
//MMAP2
	int fd_mmap2;
	printf("Voy a abrir fich2\n");
	fd_mmap2= open("Datos_Compartidos2.txt", O_CREAT|O_TRUNC|O_RDWR, 0666);
	if (fd_mmap2 < 0) {
           perror("Error creación fichero destino2");
           exit(1);
        }
	printf("Voy a escribir en fich2\n");
	write(fd_mmap2,&datoscomp2,sizeof(datoscomp2));
	mem2=static_cast<DatosMemCompartida*> (mmap(NULL,sizeof(datoscomp2),PROT_READ|PROT_WRITE,MAP_SHARED,fd_mmap2,0));
	if(mem2==MAP_FAILED){
        perror("mmap2");
        return;
        }
	
	close(fd_mmap2);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo_Cliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	print(cad,0,0,1,1,1);
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	disparo1.Dibuja();
	disparo2.Dibuja();
	
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo_Cliente::OnTimer(int value)
{	

	//Leo socket del servidor
	
	s.Receive(cadena1, sizeof(cadena1));
	sscanf(cadena1,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); 
	
	//printf("Informacion obtenida del servidor\n jugx:%f jugy:%f\n",jugador2.x1,jugador2.y1);
	
	contador+=25;
	
	for(int i;i<strlen(cadena1);i++)
        cadena1[i]='\0';
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	disparo1.Mueve(0.025f);
	disparo2.Mueve(0.025f);
	jugador1.CoolDown(disparo2);
	jugador2.CoolDown(disparo1);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	}
	


	//Leo la accion del bot	

	if(mem->accion==-1){OnKeyboardDown('s', 0, 0);}
	else if(mem->accion==0){}
	else if(mem->accion==1){OnKeyboardDown('w', 0, 0);}
	
	if(contador>=10000){
	if(mem2->accion==-1){flag_nobot=0;OnKeyboardDown('l', 0, 0);}
	else if(mem2->accion==0){flag_nobot=0;}
	else if(mem2->accion==1){flag_nobot=0;OnKeyboardDown('o', 0, 0);}
	}
	//Actualizo los datos de la memoria compartida
	mem->esfera=esfera;
	mem->raqueta1=jugador1;
	mem2->esfera=esfera;
	mem2->raqueta1=jugador2;
	

	
	
	//Salgo del juego si los puntos son 3
	if((puntos1==3)||(puntos2==3)){
	exit(1);
	}
	
}

void CMundo_Cliente::OnKeyboardDown(unsigned char key, int x, int y)
{

	char tecla[]="0";
	
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':sprintf(tecla,"s");break;
	case 'w':sprintf(tecla,"w"); break;
	case 'l':
		if(flag_nobot==1){
		contador=0;
		}
		sprintf(tecla,"l");break;
	break;
	case 'o':
		if(flag_nobot==1){
		contador=0;
		}
		sprintf(tecla,"o");break;
	case 'e':
	/*if(jugador1.getDistancia(disparo1)>=24){
		disparo1.setPos(jugador1.getPos().x, jugador1.getPos().y);
		disparo1.setRadio(0.25);
		disparo1.setVel(6, 0);
	}*/
		sprintf(tecla,"e");break;
	case 'p':
	/*if(jugador2.getDistancia(disparo2)>=24){
		disparo2.setPos(jugador2.getPos().x, jugador2.getPos().y);
		disparo2.setRadio(0.25);
		disparo2.setVel(-6, 0);
	}*/
		sprintf(tecla,"p");break;
	}
	flag_nobot=1;
	
	s.Send(tecla,sizeof(tecla));
	
}

void CMundo_Cliente::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	//Creamos Socket
	char ip[]="127.0.0.1";
	char nombre[100];
	printf("Inserte nombre del cliente: \n");
	scanf("%s",nombre);
	printf("Conectando...\n");
	s.Connect(ip,4200);
	printf("Conectado!\n");
	sleep(0.01);
	s.Send(nombre,sizeof(nombre));
	
	
}
