//Code autor: Fco. Javier Valero Cuadrado.

// Mundo.cpp: implementation of the CMundo_Servidor class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <signal.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
void* hilo_comandos1(void* d);
void* hilo_comandos2(void* d);
void* hilo_servidor(void *d);
using namespace std;
//FUNCION PARA CERRAR EL SERVIDOR QUE SE EJECUTA CUANDO LLEGA SEÑAL SIGINT, SIGTERM Y SIGPIPE 

static void CerrarServidorERROR (int n)
{
	printf("Error cierra el servidor\n");
	exit(1);
}
//SIGUSR2
static void CerrarServidorCorrecto (int n)
{
	printf("Servidor cerrado correctamente\n");
	exit(1);
}

CMundo_Servidor::CMundo_Servidor()
{
	Init();
	disparo1.setPos(-300, -300);
	disparo1.setRadio(0);
	disparo1.setVel(0, 0);
	disparo2.setPos(-300, -300);
	disparo2.setRadio(0);
	disparo2.setVel(0, 0);
}

CMundo_Servidor::~CMundo_Servidor()
{
	close(fd);
	s_client.clear();	
	acabar=1;
	pthread_join(thid1,NULL);
	printf("thid1 vuelve\n");
	pthread_join(thid2,NULL);
	printf("thid2 vuelve\n");
	pthread_join(thid_socket,NULL);
	printf("thid_Socket vuelve\n");
}

void CMundo_Servidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
	


}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo_Servidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	print(cad,0,0,1,1,1);
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	disparo1.Dibuja();
	disparo2.Dibuja();
	
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo_Servidor::OnTimer(int value)
{	

        if(s_client.size()<2){
		puntos1=0;
		puntos2=0;
	}
	
	contador+=25;
	
	sprintf(cadena2,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	
	
	for (int i=s_client.size()-1; i>=0; i--) {
         if (s_client[i].Send(cadena2,sizeof(cadena2)) <= 0) {
            s_client.erase(s_client.begin()+i);
            printf("Cliente eliminado\n");
         }
     	}
        
	
	/*for(int i=0;i<s_client.size();i++)
	s_client[i].Send(cadena2, sizeof(cadena2));*/
	
	
	for(int i;i<strlen(cadena);i++)
        cadena[i]='\0';
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	disparo1.Mueve(0.025f);
	disparo2.Mueve(0.025f);
	jugador1.CoolDown(disparo2);
	jugador2.CoolDown(disparo1);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		sprintf(cadena, "%s lleva un total de %d puntos\n",Jug2,puntos2);
		write(fd, cadena, strlen(cadena));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		sprintf(cadena, "%s lleva un total de %d puntos\n",Jug1,puntos1);
		write(fd, cadena, strlen(cadena));
	}
	if((puntos1==3)||(puntos2==3)){
	exit(1);
	}
	
}

void CMundo_Servidor::OnKeyboardDown(unsigned char key, int x, int y)
{
/*	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;
		if(flag_nobot==1){
		contador=0;
		}
	break;
	case 'o':jugador2.velocidad.y=4;
		if(flag_nobot==1){
		contador=0;
		}
		break;
	case 'e':
	if(jugador1.getDistancia(disparo1)>=24){
		disparo1.setPos(jugador1.getPos().x, jugador1.getPos().y);
		disparo1.setRadio(0.25);
		disparo1.setVel(6, 0);
	}
		break;
	case 'p':
	if(jugador2.getDistancia(disparo2)>=24){
		disparo2.setPos(jugador2.getPos().x, jugador2.getPos().y);
		disparo2.setRadio(0.25);
		disparo2.setVel(-6, 0);
	}
		break;
	}
	flag_nobot=1;*/
}

void CMundo_Servidor::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	
	contador=0;
//Accedemos al logger
	if(rf=fork()==0){
		execlp("./logger","logger",logger, NULL);
		perror("exec");
		return ;
	}else if (rf==-1){
		perror("fork");
		return;
	}else
	sleep(1);
	fd=open(logger,O_WRONLY);
	//printf("Abierto y escribo\n");
	sprintf(cadena, "ARCHIVO LOGGER-- RECOGIDA PUNTUACION\n");
		write(fd, cadena, strlen(cadena));
		



//SEÑALES
//ARMADO DE LA SEÑAL SIGPIPE

	struct sigaction accion;
	accion.sa_handler=&CerrarServidorERROR;
	//act.sa_flags = SA_SIGINFO;
	if (sigaction(SIGPIPE, &accion, NULL) < 0) {
		perror ("sigaction");
		return;
	}
	if (sigaction(SIGTERM, &accion, NULL) < 0) {
		perror ("sigaction");
		return;
	}
	if (sigaction(SIGINT, &accion, NULL) < 0) {
		perror ("sigaction");
		return;
	}
	struct sigaction accion2;
	accion2.sa_handler=&CerrarServidorCorrecto;
	if (sigaction(SIGUSR2, &accion2, NULL) < 0) {
		perror ("sigaction");
		return;
	}
	//-------------Socket------------------------
	
	char ip[]="127.0.0.1";	
	if(s.InitServer(ip,4200)==-1)
	printf("error abriendo el servidor\n");
	char nombre[100];
	printf("Esperando Conexion\n");
	s_client.push_back(s.Accept());
	//RECIBIMOS EL NOMBRE DEL CLIENTE
	s_client[s_client.size()-1].Receive(nombre,sizeof(nombre));
	printf("%s se ha conectado a la partida.\n",nombre);
	
	//creamos el thread de los sockets
	pthread_create(&thid_socket, NULL, hilo_servidor, this);
	printf("thread socket\n");	
		
//creamos el thread
pthread_create(&thid1, NULL, hilo_comandos1, this);
printf("thread creado\n");

}

void* hilo_comandos1(void* d)
{
      CMundo_Servidor* p=(CMundo_Servidor*) d;
      p->RecibeComandosJugador1();
}

void* hilo_comandos2(void* d)
{
      CMundo_Servidor* p=(CMundo_Servidor*) d;
      p->RecibeComandosJugador2();
}

void CMundo_Servidor::RecibeComandosJugador1()
{
	printf("Escuchando teclas jugador 1");
     while (!acabar) {
            usleep(10);
        if(s_client.size()>=2){
            char cad1[100];
            unsigned char key1='\0';
            s_client[0].Receive(cad1,sizeof(cad1));
            sscanf(cad1,"%c",&key1);
         
     switch(key1) {

	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	
	case 'e':
	if(jugador1.getDistancia(disparo1)>=24){
		disparo1.setPos(jugador1.getPos().x, jugador1.getPos().y);
		disparo1.setRadio(0.25);
		disparo1.setVel(6, 0);
	}
		break;
	}
	}
      }
      
}

void CMundo_Servidor::RecibeComandosJugador2()
{
	printf("Escuchando teclas jugador 2\n");
     while (!acabar) {
            usleep(10);
        if(s_client.size()>=2){
            char cad2[100];
            unsigned char key2='\0';

	 s_client[1].Receive(cad2,sizeof(cad2));
         sscanf(cad2,"%c",&key2);
		//printf("key2: %c\n",key2);
	switch(key2) {
		case 'l':jugador2.velocidad.y=-4;break;
		case 'o':jugador2.velocidad.y=4;break;
	       case 'p':
			if(jugador2.getDistancia(disparo2)>=24){
			disparo2.setPos(jugador2.getPos().x, jugador2.getPos().y);
			disparo2.setRadio(0.25);
			disparo2.setVel(-6, 0);
			}
		break;
	}
      }
      
    }
}

void* hilo_servidor(void* d)
{
      CMundo_Servidor* p=(CMundo_Servidor*) d;
      p->GestionaConexiones();
}

void CMundo_Servidor::GestionaConexiones(){
	while(!acabar){
		Socket s_communication;
		char nombre[100];
		printf("Esperando Conexion\n");
		s_communication=s.Accept();
		s_client.push_back(s_communication);
		//RECIBIMOS EL NOMBRE DEL CLIENTE
		s_client[s_client.size()-1].Receive(nombre,sizeof(nombre));
		printf("%s se ha conectado a la partida.\n",nombre);
		if(s_client.size()==2)
		pthread_create(&thid2, NULL, hilo_comandos2, this);
	}
}
